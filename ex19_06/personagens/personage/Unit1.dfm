object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 201
  ClientWidth = 447
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 40
    Top = 13
    Width = 27
    Height = 13
    Caption = 'Nome'
  end
  object Label2: TLabel
    Left = 40
    Top = 64
    Width = 44
    Height = 13
    Caption = 'Profissao'
  end
  object Label3: TLabel
    Left = 40
    Top = 112
    Width = 23
    Height = 13
    Caption = 'Nivel'
  end
  object Label4: TLabel
    Left = 272
    Top = 16
    Width = 35
    Height = 13
    Caption = 'Ataque'
  end
  object Label5: TLabel
    Left = 272
    Top = 61
    Width = 34
    Height = 13
    Caption = 'Defesa'
  end
  object Label6: TLabel
    Left = 272
    Top = 112
    Width = 20
    Height = 13
    Caption = 'Vida'
  end
  object Edit1: TEdit
    Left = 40
    Top = 32
    Width = 121
    Height = 21
    TabOrder = 0
    Text = 'Edit1'
  end
  object Edit2: TEdit
    Left = 40
    Top = 80
    Width = 121
    Height = 21
    TabOrder = 1
    Text = 'Edit2'
  end
  object Edit3: TEdit
    Left = 40
    Top = 123
    Width = 121
    Height = 21
    TabOrder = 2
    Text = 'Edit3'
  end
  object Edit4: TEdit
    Left = 272
    Top = 32
    Width = 121
    Height = 21
    TabOrder = 3
    Text = 'Edit4'
  end
  object Edit5: TEdit
    Left = 272
    Top = 80
    Width = 121
    Height = 21
    TabOrder = 4
    Text = 'Edit5'
  end
  object Edit6: TEdit
    Left = 272
    Top = 123
    Width = 121
    Height = 21
    TabOrder = 5
    Text = 'Edit6'
  end
  object Button1: TButton
    Left = 120
    Top = 168
    Width = 75
    Height = 25
    Caption = 'Atualizar'
    TabOrder = 6
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 240
    Top = 168
    Width = 75
    Height = 25
    Caption = 'Limpar'
    TabOrder = 7
  end
  object NetHTTPClient1: TNetHTTPClient
    Asynchronous = False
    ConnectionTimeout = 60000
    ResponseTimeout = 60000
    AllowCookies = True
    HandleRedirects = True
    UserAgent = 'Embarcadero URI Client/1.0'
    Left = 32
    Top = 160
  end
end
